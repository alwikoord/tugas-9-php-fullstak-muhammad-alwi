<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $animal = new Animal("Shaun");
    echo "Nama Hewan : " . $animal->nama_hewan . "<br>";
    echo "Jumlah Kaki : " . $animal->kaki . "<br>";
    echo "Cold Blooded : " . $animal->cold . "<br>";

    echo "<br>";

    $frog = new Frog("buduk");
    echo "Nama Hewan : " . $frog->nama_hewan . "<br>";
    echo "Jumlah Kaki : " . $frog->kaki . "<br>";
    echo "Cold Blooded : " . $frog->cold . "<br>";
    echo $frog->gasskeun();

    echo "<br> <br>";

    $ape = new Ape("Kera Sakti");
    echo "Nama Hewan : " . $ape->nama_hewan . "<br>";
    echo "Jumlah Kaki : " . $ape->kaki . "<br>";
    echo "Cold Blooded : " . $ape->cold . "<br>";
    echo $ape->apeyell();
    
?>